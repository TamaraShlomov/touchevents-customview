package il.co.hyperactive.touchevents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView coordinate, eventtext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        coordinate = (TextView)findViewById(R.id.textViewCoordinate);
        eventtext = (TextView)findViewById(R.id.textViewEvent);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
            switch (event.getAction())
            {
                case MotionEvent.ACTION_UP:
                    eventtext.setText("Action Up");
                    break;
                case MotionEvent.ACTION_DOWN:
                    eventtext.setText("Action Down");
                    break;
                case MotionEvent.ACTION_MOVE:
                    eventtext.setText("Action Move");
                    break;
                case MotionEvent.ACTION_CANCEL:
                    eventtext.setText("Action Cancel");
                    break;
            }
        coordinate.setText(event.getX() + ", " + event.getY());
        return true;
    }
}
